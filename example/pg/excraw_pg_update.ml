open Excraw_pg_tables

let () =
  let database = Option.value ~default:Db_env.database (Sys.getenv_opt "PGDATABASE") in
  let upgrade0 dbh version =
    EzPG.upgrade ~dbh ~version ~downgrade:Crawlori.downgrade Crawlori.upgrade in
  let upgrade1 dbh version =
    EzPG.upgrade ~dbh ~version ~downgrade:(Pg_bigmap_tables.downgrade ~keep:false)
      (Pg_bigmap_tables.upgrade ~keep:false) in
  let upgrades = [ 0, upgrade0; 1, upgrade1] in
  EzPGUpdater.main database ~upgrades
