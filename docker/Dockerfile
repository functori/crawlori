FROM registry.gitlab.com/functori/dev/docker-images/opam-postgres-debian:latest as compilation

COPY --chown=functori . ./crawlori
WORKDIR ./crawlori
RUN $(pg_config --bindir)/pg_ctl start -D ../data && \
opam switch create --no-install . ocaml-system && \
eval $(opam env) && \
make pg-deps && \
make clean && \
make

FROM debian:latest as target
LABEL Description="crawlori container" Vendor="Functori"

RUN apt-get update && \
apt-get install -y sudo postgresql libcurl4-gnutls-dev && \
addgroup --system functori && adduser --system --group --shell /bin/sh functori && \
echo 'functori ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
sudo chown functori:functori /var/run/postgresql/

USER functori
WORKDIR /home/functori
RUN mkdir data && $(pg_config --bindir)/initdb -D data -U functori && \
echo 'host all  all    0.0.0.0/0  trust' >> data/pg_hba.conf && \
echo "listen_addresses='*'" >> data/postgresql.conf

COPY --from=compilation --chown=functori /home/functori/crawlori/config.json /home/functori/crawlori/docker/entrypoint.sh /home/functori/crawlori/_bin/crawlori /home/functori/crawlori/_build/default/src/pg/update.exe /home/functori/

RUN $(pg_config --bindir)/pg_ctl start -D data && ./update.exe
EXPOSE 5432
VOLUME  ["/home/functori/data"]
ENTRYPOINT [ "./entrypoint.sh" ]
CMD [ "config.json" ]
