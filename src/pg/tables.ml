(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

let downgrade = [
  "drop table predecessors";
]

let upgrade = [
  "create table predecessors(\
   hash varchar primary key, \
   predecessor varchar not null, \
   level int not null, \
   main boolean not null default false)";
]

let merge l =
  let l = List.fold_left (fun acc (v, (u, d)) ->
      match List.assoc_opt v acc with
      | None -> (v, (u, d)) :: acc
      | Some (u0, d0) ->
        let acc = List.remove_assoc v acc in
        (v, (u0 @ u, d0 @ d)) :: acc) [] l in
  List.sort (fun (v1, _) (v2, _) -> Int.compare v1 v2) l

(* {|create type balance_update_kind as enum (
 *   'contract', 'rewards', 'fees', 'deposits')|}; *)
(* {|create table balance_updates(
 *   id bigserial,
 *   account varchar not null,
 *   change zarith not null,
 *   kind balance_update_kind not null,
 *   cycle int,
 *   block varchar not null,
 *   level int not null,
 *   tsp timestamp not null,
 *   main boolean not null,
 *   operation varchar,
 *   index int)|}; *)
(* {|create index balance_updates_account_index on balance_updates(account)|};
 * {|create index balance_updates_block_index on balance_updates(block)|};
 * {|create index balance_updates_level_index on balance_updates(level)|};
 * {|create index balance_updates_operation_index on balance_updates(operation)|}; *)
