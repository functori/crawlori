# Tezos Crawler

Crawlori is a powerful crawler developed by Functori.
It allows to crawl Tezos blockchain in a very fast and modular way for different backends.

## 1. Install

Installation can be done via opam:
```bash
opam pin add crawlori.~dev git+https://gitlab.com/functori/dev/crawlori
```

## 2. Backends

To use this crawler, you can choose among different backends (3 unix and 1 web):

 - [postgresql](https://www.postgresql.org)
 - [lmdb](http://symas.com/lmdb)
 - [indexedb](https://developer.mozilla.org/fr/docs/Web/API/IndexedDB_API)
 - [caqti](https://github.com/paurkedal/ocaml-caqti) (can be used with several backend)

## 3. Backend install

### Postgresql

Before installing the OCaml binding of postgresql, you need to install postgresql itself by following the online [documentation](https://www.postgresql.org).

To use postgresql with ocaml, you will to setup a user and configure it (requiring superuser).
```bash
sudo -i -u postgres -- psql -c 'create user ${USER} with createdb;'
```
Due to sandbox issues, the sandboxing of opam needs to be removed in the opam config file `~/.opam/config` by default.

After the configuration, you can install [PGOCaml](https://github.com/darioteixeira/pgocaml), in addition to other project dependencies. This can be achieved with:
```bash
opam install ocurl pgocaml pgocaml_ppx ez_pgocaml
```

### LMDB

You only need to install the ocaml binding for lmdb, in addition to other project dependencies. This can be achieved with:

```bash
opam install ocurl lmdb
```

### CAQTI

You only need to install the caqti-lwt and the caqti driver of your choice. This can be achieved with:

```bash
opam install ocurl caqti-lwt
```

### Indexedb

You only need to install the ocaml binding for indexedDB, in addition to other project dependencies. This can be achieved with:

```bash
opam install ezjs_idb js_of_ocaml-lwt
```

## 4. Plugins

The program provided in each backend only deal with getting the blocks and handling possible reorganizations. To use crawlori for your own needs you need to supplement plugins.
These plugins can be provided with a module with the signature:
```ocaml
type txn                 (* transaction type (for example the connection handler of a database) *)
type extra               (* config type for this plugin *)
type init_acc            (* initial accumulator type (common with the backend type: Pg.init_acc, Idb.init_acc, ...) *)
val name : string        (* name of the plugin (to add to the config if needed) *)
val always : bool        (* flag to choose if the plugin should always be run or only if present in config *)
val init : extra -> init_acc -> (init_acc, error) result Lwt.t
                         (* initialisation function: useful to create new tables or setting up some config *)
val register_operation : ?forward:bool -> extra -> txn -> block_op -> (unit, error) result Lwt.t
                         (* register an operation *)
val register_block : ?forward:bool -> extra -> txn -> Proto.full_block -> (unit, error) result Lwt.t
                         (* register a block *)
val set_main : ?forward:bool -> extra ->  txn -> main_arg -> (unit -> (unit, error) result Lwt.t, error) result Lwt.t
                         (* validate or unvalidate a block depending on reorganization *)
val forward_end : extra -> int32 -> (unit, error) result Lwt.t
                         (* function run at the end of a forward crawling *)
```

We will suppose the plugin is defined in a file `myPlugin.ml`.
The plugins to be registered in the main loop (for the respective backend).
For example in PGOCaml backend (let's call it `myCrawler.ml`):
```ocaml
open Crawlori

let filename = ref None

let extra_enc = Json_encoding.( ... ) (* json-data-encoding encoding for the extra fields of your config (cf later) *)

let get_config () =
  Arg.parse [] (fun f -> filename := Some f) "./myCrawler.exe conf.json";
  match !filename with
  | None -> rerr `no_config
  | Some f ->
    try rok (EzEncoding.(destruct (config_enc extra_enc) f))
    with _ ->
    try
      let ic = open_in f in
      let json = Ezjsonm.from_channel ic in
      close_in ic;
      rok Json_encoding.(destruct (config_enc extra_enc) json)
    with exn -> rerr (`cannot_parse_config exn)

let () =
  EzLwtSys.run @@ fun () ->
  Lwt.map (Result.iter_error (fun e -> print_error e; exit 1)) @@
  let>? config = get_config () in     // a function to get the config from the command line or from a file as need be
  match config.db_kind with
  | `pg ->
    let open Make(Pg)(E) in
    Plugins.register_mod (module MyPlugin);
    let>? () = init config in
    loop config
  | _ ->
    Format.eprintf "backend not handled@.";
    exit 1
```
with the `dune` file
```
(executable
 (name myCrawler)
 (modules myPlugin myCrawler)
 (libraries crawlori crawlori.pg ez_api.ezjsonm_unix))
```

## 5. Setup the configuration file

First, you need to setup a configuration file to crawl the Tezos blockchain. A
minimal example is provided in config.json file.

```json
{
  "nodes": <string list> ,    // required, list of Tezos node: for example [ "http://tz.functori.com" ]
  "start": <int>,             // optional, the start block from which you want to start crawling
  "db_kind": <string>,        // optional, the database backend you want to use ("pg", "lmdb", "idb", "caqti"), defaul value is "pg"
  "step_forward": <int>,      // optional, number of block chunks you want to crawl, default value is 100
  "sleep" : <float>,          // optional, sleep time for the head watcher
  "forward" : <int>,          // optional, the level until you want to crawl forward
  "confirmations" : <int>,    // optional, depth at which you want to acknowledge that a block is assured, default is 10
  "verbose" : <int>,          // optional, verbose level, default is 0
  "retry" : <int>,            // optional, retry calling the nodes a certain number of times
  "retry_timeout": <float>,   // optional, number of seconds before retries (default is 5 seconds)
  "no_forward": <bool>,       // optional, cancel the initial forward crawling (default is false)
  ...                         // any other fields that you need in your plugins
}
```
The only required field is the `nodes`. All other fields are optional with some default values.

## Troubleshooting

If you encouter some issues with pgocaml, you may need to disable the
sandbox mode of OPAM and reinstall it.
```bash
opam init --reinit --disable-sandboxing --bare
opam reinstall pgocaml pgocaml_ppx ez_pgocaml
```

## License
 Copyright © 2021, Functori <contact@functori.com>. Released under the
 [MIT License](https://gitlab.com/functori/crawlori/-/blob/master/LICENSE).
