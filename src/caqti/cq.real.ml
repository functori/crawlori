open Proto
open Crp
open Caqti_request.Infix

type init_acc = string list (* todo: handle downgrade *)
type txn = Caqti_lwt.connection

let pool = ref (None : (Caqti_lwt.connection, Caqti_error.t) Caqti_lwt.Pool.t option)

exception Tzfunc_exn of Crp.error

let wrap r =
  Result.map_error (fun e -> `caqti_error (Caqti_error.show e)) r

let wrap_lwt p = Lwt.map wrap p

let use f =
  match !pool with
  | None -> failwith "caqti pool not initialized"
  | Some pool ->
    wrap_lwt @@ Caqti_lwt.Pool.use f pool

let transaction : (txn -> 'a rp) -> 'a rp = fun f ->
  match !pool with
  | None -> failwith "caqti pool not initialized"
  | Some pool ->
    Lwt.catch
      (fun () ->
         let|> r = Caqti_lwt.Pool.use (fun (module C : Caqti_lwt.CONNECTION) ->
             C.with_transaction (fun () ->
                 let> r = f (module C: Caqti_lwt.CONNECTION) in
                 match r with
                 | Ok x -> rok x
                 | Error e -> Lwt.fail (Tzfunc_exn e))) pool in
         wrap r)
      (function (Tzfunc_exn e) -> rerr e | exn -> rerr (`caqti_exn exn))

let init ?hook ?n () =
  let max_size = match n with
    | Some n -> Some n
    | None -> match Sys.getenv_opt "CQLWTMAXCONNEXIONS" with
      | None -> None
      | Some s -> try Some (int_of_string s) with _ -> None in
  (match Caqti_lwt.connect_pool ?max_size (Uri.of_string Crp.database) with
   | Ok p -> pool := Some p
   | Error err -> failwith (Caqti_error.show err));
  let q = (Caqti_type.unit ->. Caqti_type.unit)
      "create table if not exists caqti_info(name varchar primary key, value varchar not null)" in
  let>? () = use @@ fun (module C) -> C.exec q () in
  let q = (Caqti_type.unit ->. Caqti_type.unit)
      "insert into caqti_info(name, value) values('version', '0') on conflict do nothing" in
  let>? () = use @@ fun (module C) -> C.exec q () in
  let q0 =
    "create table if not exists predecessors(\
     hash varchar primary key, \
     predecessor varchar not null, \
     level int not null, \
     main boolean not null default false)" in
  let>? upgrades = match hook with
    | None -> rok [ q0 ]
    | Some f -> f [ q0 ] in
  let q = (Caqti_type.unit ->! Caqti_type.string) "select value from caqti_info where name = 'version'" in
  let>? v = use @@ fun (module C) -> C.find q () in
  let v = int_of_string v in
  let|>? _ = fold (fun i s ->
      if i >= v then
        let q = (Caqti_type.unit ->. Caqti_type.unit) s in
        let>? () = use @@ fun (module C) -> C.exec q () in
        let q = (Caqti_type.unit ->. Caqti_type.unit) "update caqti_info set value = (value::int + 1)::varchar where name = 'version'" in
        let>? () = use @@ fun (module C) -> C.exec q () in
        rok (i+1)
      else rok (i+1)
    ) 0 upgrades in
  ()

let registered hash =
  let q = (Caqti_type.string ->* Caqti_type.bool)
      "select main from predecessors where hash = ?" in
  use @@ fun (module C) ->
  let|>? l = C.collect_list q hash in
  match l with
  | [ main ] -> true, main
  | _ -> false, false

let head dbh =
  let q = (Caqti_type.unit ->! Caqti_type.(tup2 string int32))
      "select hash, level from predecessors where main order by level desc limit 1" in
  let aux (module C : Caqti_lwt.CONNECTION) = C.find_opt q () in
  match dbh with
  | None ->
    let|> r = use aux in
    begin match r with Ok (Some x) -> Ok x | _ -> Ok ("", 0l) end
  | Some dbh ->
    let|> r = aux dbh in
    match r with Ok (Some x) -> Ok x | _ -> Error `empty_database

let recursion (module C: Caqti_lwt.CONNECTION) hash =
  let q = (Caqti_type.string ->* Caqti_type.(tup2 string bool))
      "select predecessor, main from predecessors where hash = ?" in
  wrap_lwt @@ C.collect_list q hash

let register_predecessor ~dbh:(module C : Caqti_lwt.CONNECTION) b =
  let q = (Caqti_type.(tup3 string string int32) ->! Caqti_type.string)
      "insert into predecessors(hash, predecessor, level, main) \
       values(?, ?, ?, false) \
       on conflict do nothing returning hash" in
  let> r = C.find_opt q (b.hash, b.header.shell.predecessor, b.header.shell.level) in
  match r with
  | Ok (Some _) -> rok `new_block
  | _ ->
    let q = (Caqti_type.string ->! Caqti_type.bool)
        "select main from predecessors where hash = ?" in
    let|> r = C.find_opt q b.hash in
    match r with
    | Ok (Some main) -> Ok (`already_known main)
    | _ -> Error `wrong_db_state

let register ?block_hook ?operation_hook b =
  transaction @@ fun dbh ->
  let>? r = register_predecessor ~dbh b in
  match r with
  | `already_known main ->
    if main then rok `main else rok `alt
  | `new_block ->
    let block = Option.map (fun f -> (fun () b -> f dbh b)) block_hook in
    let operation = Option.map (fun f -> (fun () o -> f dbh o)) operation_hook in
    let|>? (), () = Utils.Process.block ?block ?operation ((), ()) b in
    `new_block

let set_main ?main ?hook (module C : Caqti_lwt.CONNECTION) hash =
  let main = match main with Some _ -> true | None -> false in
  let q = (Caqti_type.(tup2 bool string) ->. Caqti_type.unit)
      "update predecessors set main = ? where hash = ?" in
  let>? () = wrap_lwt @@ C.exec q (main, hash) in
  match hook with
  | None -> rok (fun () -> rok ())
  | Some f -> f (module C : Caqti_lwt.CONNECTION) {m_hash = hash; m_main = main}

let forward_end ?hook level =
  match hook with
  | None -> rok ()
  | Some f -> f level
