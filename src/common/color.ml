(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

type t = [
  | `default
  | `black
  | `red
  | `green
  | `yellow
  | `blue
  | `magenta
  | `cyan
  | `light_grey
  | `dark_grey
  | `light_red
  | `light_green
  | `light_yellow
  | `light_blue
  | `light_magenta
  | `light_cyan
  | `white
]

let to_unix_int : t -> int = function
  | `default -> 39
  | `black -> 30
  | `red -> 31
  | `green -> 32
  | `yellow -> 33
  | `blue -> 34
  | `magenta -> 35
  | `cyan -> 36
  | `light_grey -> 37
  | `dark_grey -> 90
  | `light_red -> 91
  | `light_green -> 92
  | `light_yellow -> 93
  | `light_blue -> 94
  | `light_magenta -> 95
  | `light_cyan -> 96
  | `white -> 97

let to_css : t -> string = function
  | `default -> "white"
  | `black -> "black"
  | `red -> "red"
  | `green -> "green"
  | `yellow -> "yellow"
  | `blue -> "blue"
  | `magenta -> "magenta"
  | `cyan -> "cyan"
  | `light_grey -> ""
  | `dark_grey -> ""
  | `light_red -> ""
  | `light_green -> ""
  | `light_yellow -> ""
  | `light_blue -> ""
  | `light_magenta -> ""
  | `light_cyan -> ""
  | `white -> ""
