open Crp
open Proto
open Pg_aux

type bigmap_info = {
  bi_id: Z.t; [@encoding A.zarith_enc.json]
  bi_key: Mtyped.stype; [@encoding Mtyped.stype_enc.json]
  bi_value: Mtyped.stype; [@encoding Mtyped.stype_enc.json]
  bi_name: string;
} [@@deriving encoding]

module SMap = Map.Make(String)

module Make(S : sig
    val always : bool
    val keep : bool
    val fields : (string * (Mtyped.stype * Mtyped.stype) option) list
  end) = struct

  type nonrec txn = txn

  let always = S.always
  let name = "bigmaps"
  let forward_end _config _level = rok ()
  let register_operation ?forward:_ _config _dbh _op = rok ()

  let get_contracts () =
    handle @@ fun dbh ->
    let|>? l = [%pgsql.object dbh
        "select address, bigmap, name, key, value from contracts where main"] in
    List.map (fun r ->
        r#address, {
          bi_id = Z.of_string r#bigmap; bi_name = r#name;
          bi_key = EzEncoding.destruct Mtyped.stype_enc.json r#key;
          bi_value = EzEncoding.destruct Mtyped.stype_enc.json r#value }) l

  let init config acc =
    let acc = acc @ [0, (Pg_bigmaps_tables.upgrade ~keep:S.keep, Pg_bigmaps_tables.downgrade)] in
    let> r = get_contracts () in
    let contracts = match r with | Ok contracts -> contracts | _ -> [] in
    let contracts = List.fold_left (fun acc (k, v) -> SMap.add k v acc) config#contracts contracts in
    config#set_contracts contracts;
    rok acc

  let bigmap_updates info l =
    List.flatten @@
    List.filter_map (function
        | Big_map { id; diff = SDUpdate l }
        | Big_map { id; diff = SDAlloc {updates=l; _} } when id = info.bi_id ->
          Some (List.filter_map (fun {bm_key; bm_value; bm_key_hash} ->
              match Mtyped.parse_value info.bi_key bm_key with
              | Error _ -> None
              | Ok k ->
                match bm_value with
                | None -> Some (bm_key_hash, k, None)
                | Some value ->
                  match Mtyped.parse_value info.bi_value value with
                  | Error _ -> None
                  | Ok v -> Some (bm_key_hash, k, Some v)) l)
        | _ -> None) l

  let bigmap_allocs ?external_diff l =
    let f id key_type value_type =
      match Mtyped.parse_type key_type, Mtyped.parse_type value_type with
      | Error _, _ | _, Error _ -> id, None
      | Ok k, Ok v ->
        id, Some (Mtyped.short k, Mtyped.short v) in
    List.sort (fun (id1, _) (id2, _) -> compare id1 id2) @@
    List.filter_map (function
        | Big_map { id; diff = SDAlloc {key_type; value_type; _} } ->
          if id < Z.zero then None
          else Some (f id key_type value_type)
        | Big_map { id; diff = SDCopy {source; _} } ->
          begin match external_diff with
            | Some l ->
              List.find_map (function
                  | Big_map { id = idc; diff = SDAlloc {key_type; value_type; _} } when source = Z.to_string idc ->
                    Some (f id key_type value_type)
                  | _ -> None) l
            | _ -> None
          end
        | _ -> None) l

  let bigmap_id ~allocs bm_index k v =
    let rec aux i = function
      | [] -> None
      | _ :: t when i < bm_index -> aux (i+1) t
      | (id, t) :: _ ->
        match t with
        | Some (k2, v2) when k = k2 && v = v2 -> Some id
        | _ -> None in
    aux 0 allocs

  let code_elt p = function
    | Mseq l ->
      List.find_map
        (function
          | Mprim { prim; args = [ arg ]; _} when prim = p -> Some arg
          | _ -> None) l
    | _ -> None

  let storage_fields script =
    let open Mtyped in
    let rec aux bm_index acc = function
      | {typ=`tuple []; name = None} -> bm_index, acc
      | {typ=`tuple (h :: t); _} ->
        let bm_index, acc = aux bm_index acc h in
        aux bm_index acc {name=None; typ=`tuple t}
      | {name=Some n; typ = `big_map (k, v)} ->
        bm_index + 1, (n, Some (
            bm_index, short k, short v)) :: acc
      | {name=Some n; _} ->
        bm_index, (n, None) :: acc
      | _ -> bm_index, acc in
    match code_elt `storage script.code with
    | None -> Error `unexpected_michelson
    | Some m -> match parse_type m with
      | Error e -> Error e
      | Ok t -> Ok (List.rev @@ snd @@ aux 0 [] t)

  let match_fields ~allocs script =
    match storage_fields script with
    | Error e ->  Error e
    | Ok fields ->
      Ok (List.find_map (fun (name, expected) ->
          match List.assoc_opt name fields, expected with
          | Some (Some (bm_index, k, v)), e when e = None || e = Some (k, v) ->
            (match bigmap_id ~allocs bm_index k v with
             | None -> None
             | Some bi_id -> Some {bi_id; bi_key = k; bi_value = v; bi_name = name})
          | _ -> None) S.fields)

  let register_op ?(forward=false) config dbh external_diff op =
    match op.bo_op.kind, op.bo_meta with
    | _, None -> rerr (`generic ("archived_operation_error", "no operation metadata from this node"))
    | Origination {script; _}, Some meta ->
      let allocs = bigmap_allocs ?external_diff meta.op_lazy_storage_diff in
      let>? () = match match_fields ~allocs script with
        | Ok (Some info) ->
          let address = Tzfunc.Crypto.op_to_KT1 op.bo_hash in
          Log.(p ~color:`magenta @@ s "[bigmaps] origination %s -> %s"
                 (String.sub op.bo_hash 0 10) (String.sub address 0 10));
          let key = EzEncoding.construct Mtyped.stype_enc.json info.bi_key in
          let value = EzEncoding.construct Mtyped.stype_enc.json info.bi_value in
          let indexes =
            let i0, i1, i2 = op.bo_indexes in
            [ Some i0; Some i1; Some i2 ] in
          let>? () = [%pgsql dbh
              "insert into contracts(address, bigmap, name, key, value, \
               source, operation, index, indexes, block, level, tsp, main) \
               values($address, ${Z.to_string info.bi_id}, ${info.bi_name}, $key, \
               $value, ${op.bo_op.source}, ${op.bo_hash}, ${op.bo_index}, \
               $indexes, ${op.bo_block}, ${op.bo_level}, ${op.bo_tsp}, $forward) \
               on conflict do nothing"] in
          config#set_contracts (SMap.add address info config#contracts);
          rok ()
        | _ ->
          rok () in
      rok external_diff
    | Transaction {destination; _}, Some meta ->
      let external_diff = match op.bo_indexes with
        | (_, _, 0l) -> Some meta.op_lazy_storage_diff
        | _ -> external_diff in
      begin match SMap.find_opt destination config#contracts with
        | None -> rok external_diff
        | Some info ->
          Log.(p ~color:`magenta @@ s "[bigmaps] transaction %s to %s"
                 (String.sub op.bo_hash 0 10) (String.sub destination 0 10));
          let l = bigmap_updates info meta.op_lazy_storage_diff in
          let indexes =
            let i0, i1, i2 = op.bo_indexes in
            [ Some i0; Some i1; Some i2 ] in
          let>? () = iter (fun (hash, key, value) ->
              let key = EzEncoding.construct Mtyped.value_enc.json key in
              let value = Option.map (EzEncoding.construct Mtyped.value_enc.json) value in
              Log.(p ~color:`magenta @@ s "[bigmaps] update %s : %s" key (Option.value ~default:"none" value));
              [%pgsql dbh
                  "insert into bigmaps(contract, hash, key, value, source, \
                   operation, index, indexes, block, level, tsp, main) \
                   values($destination, $hash, $key, $?value, ${op.bo_op.source}, \
                   ${op.bo_hash}, ${op.bo_index}, $indexes, ${op.bo_block}, ${op.bo_level}, \
                   ${op.bo_tsp}, $forward) \
                   on conflict on constraint bigmaps_pkey do update \
                   set value = $?value, source = ${op.bo_op.source}, index = ${op.bo_index}, \
                   indexes = $indexes, block = ${op.bo_block}, level = ${op.bo_level}, \
                   tsp = ${op.bo_tsp}"]) l in
          rok external_diff
      end
    | _ -> rok external_diff

  let register_block ?forward config dbh b =
    let operation = register_op ?forward config dbh in
    let|>? _ = Utils.Process.block ~operation ((), None) b in
    ()

  let set_main ?(forward=false) _config dbh m =
    let>? () =
      if not forward then
        let>? () = [%pgsql dbh
            "update contracts set main = ${m.m_main} where block = ${m.m_hash}"] in
        if S.keep then
          [%pgsql dbh
              "update bigmaps set main = ${m.m_main} where block = ${m.m_hash}"]
        else
          let>? l = [%pgsql.object dbh
              "delete from bigmaps where block = ${m.m_hash} returning *"] in
          fold (fun () r ->
              let>? () =
                [%pgsql dbh
                    "update bigmaps set main = not ${m.m_main} where \
                     contract = ${r#contract} and hash = ${r#hash}"] in
              if m.m_main then
                [%pgsql dbh
                    "insert into bigmaps(contract, hash, key, value, source, \
                     operation, index, indexes, block, level, tsp, main) \
                     values(${r#contract}, ${r#hash}, ${r#key}, $?{r#value}, \
                     ${r#source}, ${r#operation}, ${r#index}, ${r#indexes}, \
                     ${r#block}, ${r#level}, ${r#tsp}, true)"]
              else rok ()) () l
      else rok () in
    rok (fun () -> rok ())

end
