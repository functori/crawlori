let downgrade ~keep =
  [ "drop index bigmap_block_index" ] @
  (if keep then [ "drop index bigmap_hash_index" ] else [] ) @
  [ "drop table bigmap" ]

let upgrade ~keep = [
  "create table bigmap(\
   hash varchar not null, \
   key jsonb not null, \
   value varchar, \
   source varchar not null, \
   operation varchar not null, \
   index int not null, \
   indexes int[] not null, \
   block varchar not null, \
   level int not null, \
   tsp timestamp not null, \
   main boolean not null)" ] @
  if keep then [
    "alter table bigmap add constraint bigmap_id unique (hash, block, index)";
    "create index bigmap_hash_index on bigmap(hash)"
  ] else [
    "alter table bigmap add constraint bigmap_id primary key (hash, main)";
  ]
