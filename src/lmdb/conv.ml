(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2021 Functori - contact@functori.com                        *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Proto

exception Binary_conv of Tzfunc.Rp.error

type table_key = [
  | `info of string [@obj1 "info"]
  | `pred of A.block_hash [@obj1 "pred"]
  | `blocks of A.block_hash [@obj1 "blocks"]
  | `level of int32 [@obj1 "level"]
  | `head
  | `operations of (A.block_hash * A.operation_hash) [@obj1 "operations"]
] [@@deriving encoding]

type table_value = [
  | `info of string [@obj1 "info"]
  | `pred of (A.block_hash * int32 * bool) [@obj1 "pred"]
  | `blocks of block [@obj1 "blocks"]
  | `level of (A.block_hash option * A.block_hash list) [@obj1 "level"]
  | `head of (A.block_hash * int32) [@obj1 "head"]
  | `operations of micheline manager_operation list [@obj1 "operations"]
] [@@deriving encoding]

type (_, _) link =
  | LInfo : (string, string) link
  | LPred : (A.block_hash, (A.block_hash * int32 * bool)) link
  | LBlocks : (A.block_hash, block) link
  | LHead : (unit, (A.block_hash * int32)) link
  | LLevel : (int32, A.block_hash option * A.block_hash list) link
  | LOperations : ((A.block_hash * A.operation_hash), micheline manager_operation list) link

let ser enc x = match Encoding.Bin.construct enc.Encoding.bin x with
  | Error e -> raise (Binary_conv (e :> Tzfunc.Rp.error))
  | Ok y -> y

let deser enc x = match Encoding.Bin.destruct enc.Encoding.bin x with
  | Error e -> raise (Binary_conv (e :> Tzfunc.Rp.error))
  | Ok x -> x

exception GADT_error

let get : type k v. (k, v) link -> (table_key -> table_value) -> k -> v =
  fun link f k ->
  match link with
  | LInfo -> (match f (`info k) with `info v -> v | _ -> raise GADT_error)
  | LBlocks -> (match f (`blocks k) with `blocks v -> v | _ -> raise GADT_error)
  | LHead -> (match f `head with `head v -> v | _ -> raise GADT_error)
  | LPred -> (match f (`pred k) with `pred v -> v | _ -> raise GADT_error)
  | LOperations -> (match f (`operations k) with `operations v -> v | _ -> raise GADT_error)
  | LLevel -> (match f (`level k) with `level v -> v | _ -> raise GADT_error)

let remove : type k v. (k, v) link -> (table_key -> unit) -> k -> unit =
  fun link f k ->
  match link with
  | LInfo -> f (`info k)
  | LBlocks -> f (`blocks k)
  | LHead -> f `head
  | LPred -> f (`pred k)
  | LOperations -> f (`operations k)
  | LLevel -> f (`level k)

let set : type k v. (k, v) link -> (table_key -> table_value -> unit) -> k -> v -> unit =
  fun link f k v ->
  match link with
  | LInfo -> f (`info k) (`info v)
  | LBlocks -> f (`blocks k) (`blocks v)
  | LHead -> f `head (`head v)
  | LPred -> f (`pred k) (`pred v)
  | LOperations -> f (`operations k) (`operations v)
  | LLevel -> f (`level k) (`level v)

let wu n r = Lwt.wakeup n r
let werr n e = wu n (Error e)

let get_rp : type k v. (k, v) link -> (table_key -> error:(Crp.error -> unit) -> (table_value -> unit) -> unit) -> k -> v Crp.rp =
  fun link f k ->
  match link with
  | LInfo ->
    let w, n = Lwt.wait () in
    f (`info k) ~error:(werr n)
      (function `info v -> wu n (Ok v) | _ -> werr n `gadt_error);
    w
  | LBlocks ->
    let w, n = Lwt.wait () in
    f (`blocks k) ~error:(werr n)
      (function `blocks v -> wu n (Ok v) | _ -> werr n `gadt_error);
    w
  | LHead ->
    let w, n = Lwt.wait () in
    f `head ~error:(werr n)
      (function `head v -> wu n (Ok v) | _ -> werr n `gadt_error);
    w
  | LPred ->
    let w, n = Lwt.wait () in
    f (`pred k) ~error:(werr n)
      (function `pred v -> wu n (Ok v) | _ -> werr n `gadt_error);
    w
  | LOperations ->
    let w, n = Lwt.wait () in
    f (`operations k) ~error:(werr n)
      (function `operations v -> wu n (Ok v) | _ -> werr n `gadt_error);
    w
  | LLevel ->
    let w, n = Lwt.wait () in
    f (`level k) ~error:(werr n)
      (function `level v -> wu n (Ok v) | _ -> werr n `gadt_error);
    w

let remove_rp : type k v. (k, v) link -> (table_key -> error:(Crp.error -> unit) -> (unit -> unit) -> unit) -> k -> unit Crp.rp =
  fun link f k ->
  let w, n = Lwt.wait () in
  let error e = werr n e in
  let out () = wu n (Ok ()) in
  begin match link with
    | LInfo -> f (`info k) ~error out
    | LBlocks -> f (`blocks k) ~error out
    | LHead -> f `head ~error out
    | LPred -> f (`pred k) ~error out
    | LOperations -> f (`operations k) ~error out
    | LLevel -> f (`level k) ~error out end;
  w

let set_rp : type k v. (k, v) link -> (table_key -> table_value -> error:(Crp.error -> unit) -> (unit -> unit) -> unit) -> k -> v -> unit Crp.rp =
  fun link f k v ->
  let w, n = Lwt.wait () in
  let error e = werr n e in
  let out () = wu n (Ok ()) in
  begin match link with
    | LInfo -> f (`info k) (`info v) ~error out
    | LBlocks -> f (`blocks k) (`blocks v) ~error out
    | LHead -> f `head (`head v) ~error out
    | LPred -> f (`pred k) (`pred v) ~error out
    | LOperations -> f (`operations k) (`operations v) ~error out
    | LLevel -> f (`level k) (`level v) ~error out end;
  w
