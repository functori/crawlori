module E = struct
  type extra = unit
end

module Contract = struct
  let id = Z.of_int 511
  let key = `tuple [ `address; `nat ]
  let value = `nat
  let contract = "KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton"
  let always = true
  let keep = false
end

let get_config () =
  let filename = ref None in
  Arg.parse [] (fun f -> filename := Some f) "excraw_cq.exe conf.json";
  match !filename with
  | None -> Error `no_config
  | Some f ->
    try
      let ic = open_in f in
      let s = really_input_string ic (in_channel_length ic) in
      close_in ic;
      Ok EzEncoding.(destruct (Crawlori.config_enc Json_encoding.unit) s)
    with exn -> Error (`cannot_parse_config exn)
