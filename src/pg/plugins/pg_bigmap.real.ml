open Crp
open Proto
open Pg_aux

module Make(S : sig
    val id : Z.t
    val key : Mtyped.stype
    val value : Mtyped.stype
    val contract : string
    val always : bool
    val keep : bool
  end) = struct

  type nonrec txn = txn

  let always = S.always
  let name = "bigmap"
  let forward_end _config _level = rok ()
  let register_block ?forward:_ _ _ _ = rok ()

  let init _config acc =
    rok (acc @ [0, (Pg_bigmap_tables.upgrade ~keep:S.keep, Pg_bigmap_tables.downgrade ~keep:S.keep)])

  let get_bigmap_updates l =
    List.flatten @@
    List.filter_map (function
        | Big_map { id; diff = SDUpdate l }
        | Big_map { id; diff = SDAlloc {updates=l; _} } when id = S.id ->
          Some (List.filter_map (fun {bm_key; bm_value; bm_key_hash} ->
              match Mtyped.parse_value S.key bm_key with
              | Error _ -> None
              | Ok k ->
                match bm_value with
                | None -> Some (bm_key_hash, k, None)
                | Some value ->
                  match Mtyped.parse_value S.value value with
                  | Error _ -> None
                  | Ok v -> Some (bm_key_hash, k, Some v)) l)
        | _ -> None) l

  let register_operation ?(forward=false) _config dbh op =
    match op.bo_op.kind, op.bo_meta with
    | (Delegation _ | Reveal _ | Constant _), _ | _, None -> rok ()
    | Transaction {destination; _}, _ when destination <> S.contract -> rok ()
    | Origination _, _ when Tzfunc.Crypto.op_to_KT1 op.bo_hash <> S.contract -> rok ()
    | _, Some meta ->
      Log.(p ~color:`magenta @@ s "[%s] transaction %s" name (String.sub op.bo_hash 0 10));
      let l = get_bigmap_updates meta.op_lazy_storage_diff in
      let indexes =
        let i0, i1, i2 = op.bo_indexes in
        [ Some i0; Some i1; Some i2 ] in
      fold (fun () (hash, key, value) ->
          let key = EzEncoding.construct Mtyped.value_enc.json key in
          let value = Option.map (EzEncoding.construct Mtyped.value_enc.json) value in
          Log.(p ~color:`magenta @@ s "[%s] update %s : %s" name key (Option.value ~default:"none" value));
          [%pgsql dbh
              "insert into bigmap(hash, key, value, source, operation, \
               index, indexes, block, level, tsp, main) \
               values($hash, $key, $?value, ${op.bo_op.source}, ${op.bo_hash}, \
               ${op.bo_index}, $indexes, ${op.bo_block}, ${op.bo_level}, \
               ${op.bo_tsp}, $forward) on conflict on constraint bigmap_id do update \
               set value = $?value, source = ${op.bo_op.source}, index = ${op.bo_index}, \
               indexes = $indexes, block = ${op.bo_block}, level = ${op.bo_level}, \
               tsp = ${op.bo_tsp}"]) () l

  let set_main ?(forward=false) _config dbh m =
    let>? () =
      if not forward then
        if S.keep then
          [%pgsql dbh
              "update bigmap set main = ${m.m_main} where block = ${m.m_hash}"]
        else
          let>? l = [%pgsql.object dbh
              "delete from bigmap where block = ${m.m_hash} returning *"] in
          fold (fun () r ->
              let>? () =
                [%pgsql dbh
                    "update bigmap set main = not ${m.m_main} where hash = ${r#hash}"] in
              if m.m_main then
                [%pgsql dbh
                    "insert into bigmap(hash, key, value, source, operation, \
                     index, indexes, block, level, tsp, main) \
                     values(${r#hash}, ${r#key}, $?{r#value}, ${r#source}, ${r#operation}, \
                     ${r#index}, ${r#indexes}, ${r#block}, ${r#level}, ${r#tsp}, true)"]
              else rok ()) () l
      else rok () in
    rok (fun () -> rok ())

end
